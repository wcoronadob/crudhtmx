package crud.uss.edu.pe.crud.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity(name = "infraction")
@Getter
@Setter
@NoArgsConstructor
public class Infraction {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(length = 8)
    private String dni;

    private LocalDateTime fecha;

    @Column(length = 7)
    private String placa;

    @Column(length = 200)
    private String infraction;

    private String descripcion;

}
