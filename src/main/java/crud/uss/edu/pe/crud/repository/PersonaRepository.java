package crud.uss.edu.pe.crud.repository;

import crud.uss.edu.pe.crud.models.Persona;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonaRepository extends CrudRepository<Persona, Long> {
    public List<Persona> findAllByOrderByIdAsc();

    public List<Persona> findAllByOrderByIdDesc();

}
