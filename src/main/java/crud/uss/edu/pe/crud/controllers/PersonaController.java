package crud.uss.edu.pe.crud.controllers;

import crud.uss.edu.pe.crud.models.Persona;
import crud.uss.edu.pe.crud.repository.PersonaRepository;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping(path = "/personas")
public class PersonaController {

    @Autowired
    private PersonaRepository personaRepository;

    @PostMapping(path = "/add")
    public  String addNewPersona(
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam int age ) {

        Persona p = new Persona();
        p.setFirstName(firstName);
        p.setLastName(firstName);
        p.setAge(age);
        personaRepository.save(p);
        return "guardado";
    }

    @GetMapping(path = "/htmx/get/all")
    public String htmxGetAllPersonas(Model model) {
        model.addAttribute("personas", personaRepository.findAllByOrderByIdDesc());
        return "personas/components/list";
    }

    @GetMapping(path = "/htmx/get/add")
    public String htmxFormNewPersona(Model model) {
        return "personas/components/form";
    }

    @PostMapping(path = "/htmx/get/add")
    public String htmxSaveFormNewPersona(
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") String lastName,
            @RequestParam("age") Integer age,
            HttpServletResponse response,
            Persona persona,
            Model mode) {
        persona.setFirstName(firstName);
        persona.setLastName(lastName);
        persona.setAge(age);

        personaRepository.save(persona);
        response.addHeader("HX-Trigger", "reloadListOfPersonas");
        return "personas/components/form";
    }

    @PostMapping(path = "/htmx/put/update/")
    public String htmxUpdatePersona(@RequestParam("id") Long id,
                                    @RequestParam("firstName") String nombre,
                                    @RequestParam("lastName") String apellido,
                                    @RequestParam("age") Integer edad,
                                    HttpServletResponse response
    ) {

        Persona persona = personaRepository.findById(id).orElseThrow();
        persona.setFirstName(nombre);
        persona.setLastName(apellido);
        persona.setAge(edad);

        personaRepository.save(persona);
        response.addHeader("HX-Trigger", "reloadListOfPersonas");
        return "personas/components/success";
    }

    @DeleteMapping(path = "/htmx/delete/remove/")
    public ResponseEntity htmxRemovePersona(@RequestParam("id") Long id) {
        try {
            Persona persona = personaRepository.findById(id).orElseThrow();
            personaRepository.delete(persona);

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("HX-Trigger", "reloadListOfPersonas");

            return new ResponseEntity<>(httpHeaders, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // Manejar la excepción apropiadamente (por ejemplo, loguearla)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/all")
    public String getAllPersonas(Model model) {
        String titulo = "Lista de Personas";
        model.addAttribute("titulo", titulo);
        return "personas/homepage";
    }

    @GetMapping(path = "/htmx/update/")
    public String actualizarPersona(@RequestParam("id") Long id, Model model) {

        Persona persona = personaRepository.findById(id).orElseThrow();

        model.addAttribute("persona", persona);

        return "personas/components/update";
    }

}
