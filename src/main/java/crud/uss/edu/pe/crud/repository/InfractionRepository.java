package crud.uss.edu.pe.crud.repository;


import crud.uss.edu.pe.crud.models.Infraction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InfractionRepository extends CrudRepository<Infraction, Long> {

    public List<Infraction> findAllByOrderByIdDesc();

}
