package crud.uss.edu.pe.crud.controllers;

import crud.uss.edu.pe.crud.models.Infraction;
import crud.uss.edu.pe.crud.repository.InfractionRepository;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Controller
@RequestMapping(path = "/infractions")
public class InfractionController {

    @Autowired
    private InfractionRepository infractionRepository;

    @GetMapping(path = "/htmx/get/all")
    public String htmxGetAllInfractions(Model model) {
        model.addAttribute("infractions", infractionRepository.findAllByOrderByIdDesc());
        return "infractions/components/list";
    }

    @GetMapping(path = "/all")
    public String getAllInfractions(Model model) {
        String titulo = "Lista de Infracciones";
        model.addAttribute("titulo", titulo);
        return "infractions/homepage";
    }

    @GetMapping(path = "/htmx/get/add")
    public String htmxFormNewInfraction(Model model) {
        return "infractions/components/form";
    }

    @PostMapping(path = "/htmx/get/add")
    public String htmxSaveFormNewInfraction(
            @RequestParam("dni") String dni,
            @RequestParam("fecha") LocalDateTime fecha,
            @RequestParam("placa") String placa,
            @RequestParam("infraccion") String infracion,
            @RequestParam("descripcion") String descripcion,
            HttpServletResponse response,
            Infraction infraction) {

        infraction.setDni(dni);
        infraction.setFecha(fecha);
        infraction.setPlaca(placa);
        infraction.setInfraction(infracion);
        infraction.setDescripcion(descripcion);

        infractionRepository.save(infraction);
        response.addHeader("HX-Trigger", "reloadListOfInfractions");
        return "infractions/components/form";
    }

    @GetMapping(path = "/htmx/update/")
    public String actualizarInfraccion(@RequestParam("id") Long id, Model model) {
        Infraction infraction = infractionRepository.findById(id).orElseThrow();
        model.addAttribute("infraccion", infraction);
        return "infractions/components/update";
    }

    @PostMapping(path = "/htmx/put/update/")
    public String htmxUpdateInfraccion(
            @RequestParam("id") Long id,
            @RequestParam("dni") String dni,
            @RequestParam("fecha") LocalDateTime fecha,
            @RequestParam("placa") String placa,
            @RequestParam("infraction") String infraction,
            @RequestParam("descripcion") String descripcion,
            HttpServletResponse response
    ) {

        Infraction inf = infractionRepository.findById(id).orElseThrow();
        inf.setDni(dni);
        inf.setFecha(fecha);
        inf.setPlaca(placa);
        inf.setInfraction(infraction);
        inf.setDescripcion(descripcion);


        infractionRepository.save(inf);
        response.addHeader("HX-Trigger", "reloadListOfInfractions");
        return "infractions/components/success";
    }

    @DeleteMapping(path = "/htmx/delete/remove/")
    public ResponseEntity htmxRemoveInfraccion(@RequestParam("id") Long id) {
        try {
            Infraction inf = infractionRepository.findById(id).orElseThrow();
            infractionRepository.delete(inf);

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("HX-Trigger", "reloadListOfInfractions");

            return new ResponseEntity<>(httpHeaders, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // Manejar la excepción apropiadamente (por ejemplo, loguearla)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
