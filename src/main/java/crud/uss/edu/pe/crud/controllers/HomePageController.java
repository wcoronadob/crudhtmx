package crud.uss.edu.pe.crud.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomePageController {

    @GetMapping(path = "/")
    public String home(Model model) {
        String message = "hola mundo!!!!";
        model.addAttribute("titular", message);
        String home = "home";
        return home;
    }

}
